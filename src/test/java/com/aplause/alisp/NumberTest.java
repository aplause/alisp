package com.aplause.alisp;


import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class NumberTest {

	@Test
	public void testSimpleNumber() {
		Double n = 3.14;
		NumberExpression ne = new NumberExpression();
		ne.evaluate(n);
		Assert.assertTrue(ne.getValue().equals(new BigDecimal(n.toString())));
	}
	
	@Test
	public void testAddExpression() {
		Integer[] vals = {1,2,3,4,5};
		
		AddExpression ae = new AddExpression();
		ae.evaluate(Arrays.asList(vals));
		Assert.assertTrue(ae.getValue().equals(new BigDecimal("15")));
		
		
	}
}
