package com.aplause.alisp;


public interface Expression {
	
	public String getType();
	
	public Object getValue();

}
