package com.aplause.alisp;

import java.math.BigDecimal;
import java.util.List;

public class AddExpression implements Expression{

	public static final String EXPRESSION_ADD = "EXPRESSION_ADD";
	private BigDecimal value = new BigDecimal(0);
	
	@Override
	public String getType() {
		return EXPRESSION_ADD;
	}
	
	@Override
	public Object getValue() {
		return value;
	}

	public void evaluate(List<Number> numbers) {
		numbers.forEach((Number n) -> {value = value.add(new BigDecimal(n.toString()));});
	}

}
