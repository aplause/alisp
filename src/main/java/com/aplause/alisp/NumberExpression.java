package com.aplause.alisp;

import java.math.BigDecimal;

public class NumberExpression implements Expression{

	public static final String EXPRESSION_NUMBER = "EXPRESSION_NUMBER";
	private BigDecimal value = new BigDecimal(0);
	
	@Override
	public String getType() {
		return EXPRESSION_NUMBER;
	}

	@Override
	public Object getValue() {
		return value;
	}
	
	public void evaluate(Number number) {
		value = new BigDecimal(number.toString());
	}

}
