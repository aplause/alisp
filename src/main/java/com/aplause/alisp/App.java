package com.aplause.alisp;

import java.io.Console;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final String PROMPT = "$--> ";
	
	private static final String EXIT = "exit";
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        Console console = System.console();
        
        while( true) {
        	String line = console.readLine(PROMPT);
        	
        	if(line.equals(EXIT)) {
        		break;
        	}
        	System.out.println(line);
        }
    }
}
